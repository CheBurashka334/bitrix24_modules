<?php

/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2024 Bitrix
 */

use Bitrix\Main;
use Bitrix\Main\Session\Legacy\HealerEarlySessionStart;

require_once(__DIR__."/start.php");

$application = Main\HttpApplication::getInstance();
$application->initializeExtendedKernel([
	"get" => $_GET,
	"post" => $_POST,
	"files" => $_FILES,
	"cookie" => $_COOKIE,
	"server" => $_SERVER,
	"env" => $_ENV
]);

if (class_exists('\Dev\Main\Migrator\ModuleUpdater'))
{
	\Dev\Main\Migrator\ModuleUpdater::checkUpdates('main', __DIR__);
}

if (!Main\ModuleManager::isModuleInstalled('bitrix24'))
{
	// wwall rules
	(new Main\Security\W\WWall)->handle();

	$application->addBackgroundJob([
		Main\Security\W\WWall::class, 'refreshRules'
	]);

	// vendor security notifications
	$application->addBackgroundJob([
		Main\Security\Notifications\VendorNotifier::class, 'refreshNotifications'
	]);
}

if (defined('SITE_ID'))
{
	define('LANG', SITE_ID);
}

$context = $application->getContext();
$context->initializeCulture(defined('LANG') ? LANG : null, defined('LANGUAGE_ID') ? LANGUAGE_ID : null);

// needs to be after culture initialization
$application->start();

// constants for compatibility
$culture = $context->getCulture();
define('SITE_CHARSET', $culture->getCharset());
define('FORMAT_DATE', $culture->getFormatDate());
define('FORMAT_DATETIME', $culture->getFormatDatetime());
define('LANG_CHARSET', SITE_CHARSET);

$site = $context->getSiteObject();
if (!defined('LANG'))
{
	define('LANG', ($site ? $site->getLid() : $context->getLanguage()));
}
define('SITE_DIR', ($site ? $site->getDir() : ''));
if (!defined('SITE_SERVER_NAME'))
{
	define('SITE_SERVER_NAME', ($site ? $site->getServerName() : ''));
}
define('LANG_DIR', SITE_DIR);

if (!defined('LANGUAGE_ID'))
{
	define('LANGUAGE_ID', $context->getLanguage());
}
define('LANG_ADMIN_LID', LANGUAGE_ID);

if (!defined('SITE_ID'))
{
	define('SITE_ID', LANG);
}

/** @global $lang */
$lang = $context->getLanguage();

//define global application object
$GLOBALS["APPLICATION"] = new CMain;

if (!defined("POST_FORM_ACTION_URI"))
{
	define("POST_FORM_ACTION_URI", htmlspecialcharsbx(GetRequestUri()));
}

$GLOBALS["MESS"] = [];
$GLOBALS["ALL_LANG_FILES"] = [];
IncludeModuleLangFile(__DIR__."/tools.php");
IncludeModuleLangFile(__FILE__);

error_reporting(COption::GetOptionInt("main", "error_reporting", E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR | E_PARSE) & ~E_STRICT & ~E_DEPRECATED & ~E_WARNING & ~E_NOTICE);

if (!defined("BX_COMP_MANAGED_CACHE") && COption::GetOptionString("main", "component_managed_cache_on", "Y") <> "N")
{
	define("BX_COMP_MANAGED_CACHE", true);
}

// global functions
require_once(__DIR__."/filter_tools.php");

/*ZDUyZmZNDljYzc1YWVjMzRiZjU2YzQ0YzBmN2UwYzNhNDAzNDM=*/$GLOBALS['_____1769269528']= array(base64_decode('R2V0TW'.'9k'.'dWxlRXZlbnRz'),base64_decode('R'.'Xhl'.'Y3'.'V0ZU1vZH'.'VsZUV'.'2Z'.'W'.'50'.'RX'.'g='));$GLOBALS['____781372918']= array(base64_decode('ZGV'.'maW5l'),base64_decode('YmFzZ'.'T'.'Y'.'0'.'X2RlY29'.'k'.'ZQ=='),base64_decode('dW'.'5zZXJ'.'pYWxpemU='),base64_decode('a'.'XNf'.'YXJyYXk'.'='),base64_decode('a'.'W5'.'fYXJ'.'yYXk='),base64_decode(''.'c2VyaWF'.'saXpl'),base64_decode('YmF'.'zZTY0X'.'2VuY29k'.'ZQ=='),base64_decode('bWt0aW1l'),base64_decode('ZGF0ZQ=='),base64_decode('ZGF'.'0'.'ZQ=='),base64_decode(''.'c'.'3Ry'.'bGVu'),base64_decode('bW'.'t0'.'aW1l'),base64_decode('ZG'.'F0ZQ=='),base64_decode(''.'ZGF0ZQ=='),base64_decode('bWV'.'0aG9kX2V4a'.'XN0cw=='),base64_decode('Y'.'2F'.'s'.'bF'.'91c2'.'Vy'.'X'.'2Z'.'1bmNfY'.'XJyYXk='),base64_decode('c3Ry'.'bGV'.'u'),base64_decode('c2Vya'.'WFsaXpl'),base64_decode('YmFzZ'.'TY0'.'X2'.'V'.'uY29k'.'Z'.'Q=='),base64_decode('c3Ryb'.'GVu'),base64_decode('aXNfYXJy'.'Y'.'Xk='),base64_decode('c2Vya'.'WFsaXpl'),base64_decode('YmFzZT'.'Y'.'0X2'.'V'.'u'.'Y2'.'9kZQ=='),base64_decode('c2'.'Vy'.'aWFsaXp'.'l'),base64_decode('YmFzZTY0X2'.'V'.'u'.'Y29k'.'ZQ=='),base64_decode('aX'.'NfY'.'XJyYXk='),base64_decode('aXNf'.'YXJyYXk='),base64_decode('aW5fYXJyYXk='),base64_decode('aW5fYXJ'.'yYX'.'k='),base64_decode(''.'b'.'W'.'t0aW1l'),base64_decode(''.'ZGF0ZQ=='),base64_decode('ZGF0Z'.'Q=='),base64_decode(''.'Z'.'GF0'.'ZQ=='),base64_decode('bW'.'t0'.'a'.'W1l'),base64_decode('ZGF0ZQ='.'='),base64_decode('ZGF0Z'.'Q='.'='),base64_decode(''.'aW5f'.'YXJyYXk'.'='),base64_decode('c2'.'VyaW'.'FsaXpl'),base64_decode('Y'.'mFzZTY0X2'.'VuY29kZQ='.'='),base64_decode('aW50dmFs'),base64_decode('dGltZQ='.'='),base64_decode('Zm'.'ls'.'ZV'.'9leGlzdHM='),base64_decode('c3Ry'.'X3JlcGxhY2U='),base64_decode('Y2xh'.'c3N'.'fZ'.'Xh'.'p'.'c'.'3R'.'z'),base64_decode('ZGVm'.'aW5l'));if(!function_exists(__NAMESPACE__.'\\___1531166732')){function ___1531166732($_1507822855){static $_1334257564= false; if($_1334257564 == false) $_1334257564=array(''.'SU5U'.'UkF'.'OR'.'VR'.'fRUR'.'J'.'VElP'.'Tg==','WQ'.'==',''.'bWFpb'.'g'.'='.'=','fm'.'NwZl9tYXB'.'fdmFsdWU'.'=','','','YW'.'xsb'.'3dlZF9j'.'bGFzc'.'2V'.'z',''.'ZQ==',''.'Zg==','ZQ='.'=','Rg'.'==','WA==','Zg==','bWFpbg'.'==','fmN'.'wZ'.'l9tYXB'.'f'.'dm'.'Fsd'.'WU=','UG9ydGFs','R'.'g==','ZQ'.'==',''.'ZQ==','W'.'A==','Rg==','RA==','RA'.'==',''.'bQ==','ZA==','WQ==','Zg='.'=','Z'.'g'.'==','Z'.'g==','Z'.'g==','UG9'.'y'.'d'.'G'.'F'.'s','R'.'g==','ZQ==','ZQ==','WA==',''.'Rg==',''.'R'.'A==','RA'.'==','bQ==','ZA==','W'.'Q==','bW'.'F'.'pbg==','T24=','U'.'2V0d'.'Glu'.'Z3NDaG'.'F'.'uZ'.'2U=','Zg==','Z'.'g==','Zg'.'='.'=','Z'.'g==',''.'bWF'.'pbg='.'=','fmNw'.'Zl9'.'tYXB'.'fd'.'mFs'.'dW'.'U=','ZQ==','ZQ'.'==',''.'R'.'A==','ZQ==','ZQ='.'=','Zg==','Z'.'g==',''.'Zg'.'='.'=','Z'.'Q==','bWFpbg='.'=','f'.'mNw'.'Zl9tYXBfd'.'mFs'.'dWU=','ZQ==','Zg==',''.'Z'.'g==','Zg'.'==','Zg='.'=',''.'bWFpbg==','fmNwZ'.'l9tY'.'XBfd'.'mFsdW'.'U=',''.'Z'.'Q==','Zg==','UG9yd'.'GFs','U'.'G9ydG'.'Fs','ZQ==',''.'ZQ==','UG9y'.'d'.'GFs','R'.'g'.'==','WA==','Rg==',''.'RA==','Z'.'Q==',''.'ZQ==','RA==','b'.'Q==','ZA'.'==','WQ==','ZQ'.'==','W'.'A==','ZQ==','Rg='.'=','ZQ='.'=','R'.'A==','Zg==','ZQ'.'='.'=','R'.'A='.'=',''.'Z'.'Q==','b'.'Q'.'==','ZA==',''.'WQ==','Zg==','Z'.'g==','Zg==','Zg==','Zg==','Zg='.'=',''.'Zg='.'=','Zg'.'==','bWFpbg==','fmN'.'wZl9tYXBf'.'dmF'.'sdWU=',''.'ZQ'.'==','ZQ'.'==',''.'UG9'.'ydGFs','Rg==','WA==','VFl'.'QRQ==','R'.'E'.'FURQ'.'==','RkVBV'.'FVSR'.'VM'.'=',''.'RVhQSVJ'.'FRA='.'=','VFlQ'.'RQ='.'=','RA==',''.'VFJZ'.'X0RBWVNf'.'Q09VT'.'lQ=','R'.'EFU'.'RQ='.'=','VFJ'.'Z'.'X'.'0R'.'BW'.'VN'.'fQ'.'09V'.'TlQ=','RVh'.'QSV'.'J'.'FR'.'A==','RkVBVF'.'VSRVM=','Zg==','Zg==','RE9'.'DVU1FTlRf'.'U'.'k'.'9PVA==','L2'.'Jpd'.'HJ'.'peC9tb2'.'R1bGV'.'zL'.'w==',''.'L2'.'luc'.'3RhbGwvaW5kZX'.'gucGhw','Lg==','X'.'w==','c2Vh'.'cmNo','Tg'.'==','','','QUNUS'.'VZ'.'F','W'.'Q==','c2'.'9ja'.'WFsbmV'.'0d29yaw==',''.'YWxsb3d'.'fZnJpZWxkcw==','WQ==',''.'SUQ=',''.'c29jaWFsbm'.'V0d2'.'9yaw==','Y'.'W'.'xsb3df'.'ZnJp'.'Z'.'Wxkcw==','S'.'U'.'Q=','c29'.'ja'.'WF'.'sb'.'mV0d29yaw'.'==','YWxsb3df'.'ZnJpZWx'.'kc'.'w='.'=','Tg==','','',''.'QUN'.'USVZF','WQ==','c2'.'9j'.'aWFsb'.'mV0d29yaw'.'='.'=','Y'.'Wx'.'sb'.'3dfbW'.'ljc'.'m9ib'.'G'.'9nX3VzZXI=','WQ==','SU'.'Q=',''.'c'.'29'.'jaWFsbmV0d'.'29yaw='.'=','YWxsb3'.'dfbWlj'.'cm9i'.'bG9nX3VzZ'.'X'.'I=','SU'.'Q=','c2'.'9jaW'.'FsbmV'.'0d'.'29y'.'aw==','YW'.'xs'.'b'.'3dfbWljcm'.'9'.'ibG9'.'nX3VzZXI=','c29'.'j'.'a'.'WF'.'sbmV'.'0d'.'2'.'9yaw==','Y'.'Wxsb3dfbWljcm9ibG9nX2dyb'.'3'.'V'.'w','WQ==',''.'SUQ=','c2'.'9j'.'a'.'WF'.'s'.'bmV'.'0d29yaw'.'==',''.'Y'.'Wxs'.'b3dfbWljcm9i'.'bG9'.'nX2dyb'.'3Vw',''.'SUQ=','c29jaWF'.'sbmV0'.'d29yaw==','Y'.'Wxs'.'b3df'.'b'.'W'.'ljcm9i'.'bG9'.'n'.'X'.'2dyb'.'3'.'V'.'w','Tg==','','','Q'.'UNUS'.'VZF','WQ==','c29jaWFsbmV0d29yaw==','YWxs'.'b3'.'d'.'fZml'.'s'.'ZXNfdXNlcg==','WQ==','S'.'UQ'.'=','c29jaWF'.'sbm'.'V0d29yaw'.'==','YWx'.'s'.'b3'.'dfZm'.'lsZXNfdXNlc'.'g==','S'.'UQ=','c29jaWFsb'.'m'.'V0d2'.'9yaw==','YWx'.'sb3dfZmls'.'ZX'.'Nf'.'dX'.'N'.'lcg==','Tg'.'==','','','QUNUSVZF','WQ==','c2'.'9jaWFsb'.'mV0'.'d29yaw='.'=','YWxsb3df'.'YmxvZ191c2Vy','WQ==','SUQ=',''.'c2'.'9'.'jaWF'.'s'.'bmV0d2'.'9'.'ya'.'w==','Y'.'Wxsb3dfYmxvZ191c2Vy','SUQ=','c29'.'jaWF'.'sbmV0'.'d'.'2'.'9yaw==','YWxsb3d'.'f'.'Ym'.'xvZ1'.'91c2Vy','Tg==','','','QU'.'NU'.'SVZF','WQ==','c29jaWFs'.'bmV0d29'.'yaw==','YWxsb3dfcGh'.'v'.'d'.'G'.'9'.'f'.'dXNlcg'.'==','WQ==','SUQ=','c'.'2'.'9jaWF'.'sbmV0'.'d29y'.'aw==','YWxs'.'b'.'3d'.'fcGh'.'vd'.'G9'.'fdXNlc'.'g==',''.'S'.'U'.'Q=',''.'c'.'29ja'.'WFs'.'bmV0d29'.'yaw==','YWxs'.'b3df'.'cGhvdG'.'9fdXNlc'.'g'.'==','Tg'.'==','','','QUNUS'.'VZ'.'F','WQ==','c'.'2'.'9ja'.'W'.'FsbmV0d'.'29yaw==','YWxsb3dfZm9ydW1'.'fd'.'XNlc'.'g==','WQ==',''.'SU'.'Q=','c'.'29j'.'aWF'.'sbm'.'V0d29'.'y'.'a'.'w==','YW'.'xsb'.'3dfZm9y'.'dW1f'.'d'.'XNlcg==','SU'.'Q=','c'.'29jaWFsbmV'.'0d29yaw==',''.'Y'.'Wxsb'.'3d'.'fZm9y'.'dW'.'1f'.'dXNlc'.'g==',''.'Tg==','','',''.'QUNUSVZF','WQ'.'==','c'.'29jaWFsbmV'.'0d29yaw==','YWxsb3dfd'.'GFza3'.'Nf'.'dXNlcg==','WQ==','SUQ=','c29'.'jaWFsbmV0d29yaw==','Y'.'Wxsb'.'3dfd'.'GFza3'.'NfdXN'.'lc'.'g'.'==',''.'SU'.'Q=',''.'c29j'.'a'.'WFsbmV0d29yaw==','YW'.'xsb3dfdG'.'Fza3NfdXNlcg==','c29j'.'aWFsbmV0d2'.'9yaw==','YWxsb3dfdG'.'F'.'z'.'a3NfZ3'.'JvdXA=',''.'WQ'.'==','SUQ=','c29j'.'aWFsbm'.'V0'.'d29yaw='.'=','YWxsb'.'3'.'dfdGFz'.'a'.'3NfZ3JvdXA'.'=','S'.'UQ=','c2'.'9jaWF'.'s'.'b'.'m'.'V0'.'d29y'.'aw='.'=',''.'YWxsb3'.'dfdGFza3NfZ3JvdXA'.'=','dGFza3'.'M=','Tg==','','','QUN'.'US'.'VZF','WQ'.'==','c29jaWFsbmV'.'0d29yaw==','YWxsb3dfY2FsZW5kYXJfdX'.'Nlcg==','WQ'.'==',''.'SUQ=','c29jaWFsbmV0d29yaw'.'==','Y'.'Wxsb3'.'d'.'f'.'Y2Fs'.'ZW5kYX'.'JfdX'.'Nlcg='.'=','SUQ=',''.'c29j'.'aW'.'Fs'.'bmV0d29yaw==','YWx'.'sb3dfY'.'2FsZW5kYXJfdXNl'.'cg='.'=',''.'c2'.'9'.'jaWFsbmV0d'.'29yaw='.'=','YW'.'xsb3dfY2FsZ'.'W5kYXJfZ3JvdXA=',''.'WQ='.'=','SUQ'.'=','c2'.'9j'.'aWFsbmV0d'.'29yaw==','YWxsb3dfY'.'2FsZW5k'.'Y'.'XJfZ3JvdX'.'A=',''.'SUQ=','c29jaWFsbmV0'.'d29ya'.'w==',''.'YWxsb'.'3dfY2'.'FsZW'.'5kYXJ'.'fZ3J'.'vdXA=','Q'.'UNUS'.'VZF','WQ==','Tg==','ZXh'.'0cmFu'.'ZXQ=','aWJsb2Nr','T25BZ'.'n'.'Rl'.'ck'.'lC'.'bG9'.'ja'.'0VsZW1'.'lbnRVcG'.'Rh'.'dGU'.'=','aW50c'.'mFuZ'.'XQ=','Q0'.'ludHJ'.'hbmV0'.'RXZl'.'b'.'n'.'RIYW'.'5kbGVycw==','U1BSZ'.'Wdp'.'c3RlclVwZGF'.'0ZW'.'RJdG'.'Vt',''.'Q0ludHJhbmV0'.'U'.'2hhcmVw'.'b2ludD'.'o6QWdlbnRM'.'aXN0'.'cygpOw='.'=','a'.'W5'.'0cmFuZ'.'XQ=','Tg'.'==','Q0ludHJh'.'bmV0U2hhcm'.'Vwb2lud'.'Do6'.'QWdlbnRR'.'dWV1'.'ZSgpOw'.'==','aW50cmF'.'uZXQ=','Tg==','Q0lu'.'dHJhbmV0'.'U2h'.'hcmVwb2lud'.'Do'.'6QW'.'dl'.'bnRVcGRh'.'dGUoKT'.'s=','aW5'.'0cmFuZXQ=','Tg==','aWJsb'.'2Nr',''.'T'.'25B'.'Z'.'n'.'RlcklCbG9j'.'a'.'0VsZ'.'W'.'1l'.'bnRB'.'ZGQ=',''.'aW'.'50cmFuZXQ'.'=','Q'.'0lud'.'HJhbmV0RXZlbnRI'.'YW5kb'.'GVycw==','U1BSZWdpc3RlclV'.'wZG'.'F0'.'ZWRJdGVt','aWJsb'.'2N'.'r','T25B'.'ZnRlcklCbG9ja'.'0VsZW'.'1lbn'.'RVcGRhdGU=','a'.'W50cmFuZXQ'.'=','Q0lu'.'dHJhb'.'mV0'.'R'.'XZ'.'lbnRIYW5kbGVyc'.'w='.'=',''.'U1BSZ'.'Wdpc'.'3'.'RlclVwZ'.'GF0ZWR'.'JdG'.'Vt','Q'.'0'.'lu'.'dH'.'Jh'.'bmV'.'0U'.'2'.'hhcm'.'Vwb2'.'ludDo6QWdlbnR'.'M'.'aXN0'.'cy'.'gpOw==',''.'aW'.'50cmFuZX'.'Q'.'=',''.'Q0lud'.'HJ'.'hbmV0'.'U'.'2hh'.'c'.'mVwb'.'2'.'ludDo6QWdl'.'bnR'.'RdWV1ZSg'.'pO'.'w==','aW50cmFuZX'.'Q=',''.'Q'.'0l'.'udHJ'.'hbmV0U2hh'.'cmVwb2lu'.'dDo6QWdlbnRVcG'.'Rhd'.'G'.'UoKT'.'s=','aW50'.'c'.'mFuZXQ=','Y'.'3Jt','b'.'WFpbg==',''.'T'.'25'.'C'.'ZWZvcmVQc'.'m9'.'sb2c=','bW'.'Fpbg='.'=','Q1dpem'.'F'.'yZFNvb'.'FBhbmVsS'.'W50'.'c'.'mFuZX'.'Q=',''.'U2hvd'.'1Bhb'.'mVs','L'.'21v'.'ZHVsZX'.'Mv'.'aW50cmFuZXQvcGFuZW'.'xfY'.'nV0dG9u'.'L'.'nBocA==','R'.'U5DT0RF','WQ==');return base64_decode($_1334257564[$_1507822855]);}};$GLOBALS['____781372918'][0](___1531166732(0), ___1531166732(1));class CBXFeatures{ private static $_1455645276= 30; private static $_1398066271= array( "Portal" => array( "CompanyCalendar", "CompanyPhoto", "CompanyVideo", "CompanyCareer", "StaffChanges", "StaffAbsence", "CommonDocuments", "MeetingRoomBookingSystem", "Wiki", "Learning", "Vote", "WebLink", "Subscribe", "Friends", "PersonalFiles", "PersonalBlog", "PersonalPhoto", "PersonalForum", "Blog", "Forum", "Gallery", "Board", "MicroBlog", "WebMessenger",), "Communications" => array( "Tasks", "Calendar", "Workgroups", "Jabber", "VideoConference", "Extranet", "SMTP", "Requests", "DAV", "intranet_sharepoint", "timeman", "Idea", "Meeting", "EventList", "Salary", "XDImport",), "Enterprise" => array( "BizProc", "Lists", "Support", "Analytics", "crm", "Controller", "LdapUnlimitedUsers",), "Holding" => array( "Cluster", "MultiSites",),); private static $_1645323014= null; private static $_1746245758= null; private static function __1538967409(){ if(self::$_1645323014 === null){ self::$_1645323014= array(); foreach(self::$_1398066271 as $_2050893273 => $_1814120840){ foreach($_1814120840 as $_667403107) self::$_1645323014[$_667403107]= $_2050893273;}} if(self::$_1746245758 === null){ self::$_1746245758= array(); $_535744355= COption::GetOptionString(___1531166732(2), ___1531166732(3), ___1531166732(4)); if($_535744355 != ___1531166732(5)){ $_535744355= $GLOBALS['____781372918'][1]($_535744355); $_535744355= $GLOBALS['____781372918'][2]($_535744355,[___1531166732(6) => false]); if($GLOBALS['____781372918'][3]($_535744355)){ self::$_1746245758= $_535744355;}} if(empty(self::$_1746245758)){ self::$_1746245758= array(___1531166732(7) => array(), ___1531166732(8) => array());}}} public static function InitiateEditionsSettings($_991293185){ self::__1538967409(); $_1858180547= array(); foreach(self::$_1398066271 as $_2050893273 => $_1814120840){ $_1838845492= $GLOBALS['____781372918'][4]($_2050893273, $_991293185); self::$_1746245758[___1531166732(9)][$_2050893273]=($_1838845492? array(___1531166732(10)): array(___1531166732(11))); foreach($_1814120840 as $_667403107){ self::$_1746245758[___1531166732(12)][$_667403107]= $_1838845492; if(!$_1838845492) $_1858180547[]= array($_667403107, false);}} $_749944996= $GLOBALS['____781372918'][5](self::$_1746245758); $_749944996= $GLOBALS['____781372918'][6]($_749944996); COption::SetOptionString(___1531166732(13), ___1531166732(14), $_749944996); foreach($_1858180547 as $_884650181) self::__1894617401($_884650181[(181*2-362)], $_884650181[round(0+0.5+0.5)]);} public static function IsFeatureEnabled($_667403107){ if($_667403107 == '') return true; self::__1538967409(); if(!isset(self::$_1645323014[$_667403107])) return true; if(self::$_1645323014[$_667403107] == ___1531166732(15)) $_1740208534= array(___1531166732(16)); elseif(isset(self::$_1746245758[___1531166732(17)][self::$_1645323014[$_667403107]])) $_1740208534= self::$_1746245758[___1531166732(18)][self::$_1645323014[$_667403107]]; else $_1740208534= array(___1531166732(19)); if($_1740208534[(870-2*435)] != ___1531166732(20) && $_1740208534[min(186,0,62)] != ___1531166732(21)){ return false;} elseif($_1740208534[min(246,0,82)] == ___1531166732(22)){ if($_1740208534[round(0+0.5+0.5)]< $GLOBALS['____781372918'][7]((187*2-374),(1324/2-662),(241*2-482), Date(___1531166732(23)), $GLOBALS['____781372918'][8](___1531166732(24))- self::$_1455645276, $GLOBALS['____781372918'][9](___1531166732(25)))){ if(!isset($_1740208534[round(0+2)]) ||!$_1740208534[round(0+0.5+0.5+0.5+0.5)]) self::__1367542550(self::$_1645323014[$_667403107]); return false;}} return!isset(self::$_1746245758[___1531166732(26)][$_667403107]) || self::$_1746245758[___1531166732(27)][$_667403107];} public static function IsFeatureInstalled($_667403107){ if($GLOBALS['____781372918'][10]($_667403107) <= 0) return true; self::__1538967409(); return(isset(self::$_1746245758[___1531166732(28)][$_667403107]) && self::$_1746245758[___1531166732(29)][$_667403107]);} public static function IsFeatureEditable($_667403107){ if($_667403107 == '') return true; self::__1538967409(); if(!isset(self::$_1645323014[$_667403107])) return true; if(self::$_1645323014[$_667403107] == ___1531166732(30)) $_1740208534= array(___1531166732(31)); elseif(isset(self::$_1746245758[___1531166732(32)][self::$_1645323014[$_667403107]])) $_1740208534= self::$_1746245758[___1531166732(33)][self::$_1645323014[$_667403107]]; else $_1740208534= array(___1531166732(34)); if($_1740208534[(217*2-434)] != ___1531166732(35) && $_1740208534[(1076/2-538)] != ___1531166732(36)){ return false;} elseif($_1740208534[(894-2*447)] == ___1531166732(37)){ if($_1740208534[round(0+1)]< $GLOBALS['____781372918'][11]((1368/2-684), min(122,0,40.666666666667), min(36,0,12), Date(___1531166732(38)), $GLOBALS['____781372918'][12](___1531166732(39))- self::$_1455645276, $GLOBALS['____781372918'][13](___1531166732(40)))){ if(!isset($_1740208534[round(0+2)]) ||!$_1740208534[round(0+1+1)]) self::__1367542550(self::$_1645323014[$_667403107]); return false;}} return true;} private static function __1894617401($_667403107, $_1821722536){ if($GLOBALS['____781372918'][14]("CBXFeatures", "On".$_667403107."SettingsChange")) $GLOBALS['____781372918'][15](array("CBXFeatures", "On".$_667403107."SettingsChange"), array($_667403107, $_1821722536)); $_834867478= $GLOBALS['_____1769269528'][0](___1531166732(41), ___1531166732(42).$_667403107.___1531166732(43)); while($_2138067277= $_834867478->Fetch()) $GLOBALS['_____1769269528'][1]($_2138067277, array($_667403107, $_1821722536));} public static function SetFeatureEnabled($_667403107, $_1821722536= true, $_1811428548= true){ if($GLOBALS['____781372918'][16]($_667403107) <= 0) return; if(!self::IsFeatureEditable($_667403107)) $_1821722536= false; $_1821722536= (bool)$_1821722536; self::__1538967409(); $_1466713223=(!isset(self::$_1746245758[___1531166732(44)][$_667403107]) && $_1821722536 || isset(self::$_1746245758[___1531166732(45)][$_667403107]) && $_1821722536 != self::$_1746245758[___1531166732(46)][$_667403107]); self::$_1746245758[___1531166732(47)][$_667403107]= $_1821722536; $_749944996= $GLOBALS['____781372918'][17](self::$_1746245758); $_749944996= $GLOBALS['____781372918'][18]($_749944996); COption::SetOptionString(___1531166732(48), ___1531166732(49), $_749944996); if($_1466713223 && $_1811428548) self::__1894617401($_667403107, $_1821722536);} private static function __1367542550($_2050893273){ if($GLOBALS['____781372918'][19]($_2050893273) <= 0 || $_2050893273 == "Portal") return; self::__1538967409(); if(!isset(self::$_1746245758[___1531166732(50)][$_2050893273]) || self::$_1746245758[___1531166732(51)][$_2050893273][(1284/2-642)] != ___1531166732(52)) return; if(isset(self::$_1746245758[___1531166732(53)][$_2050893273][round(0+0.4+0.4+0.4+0.4+0.4)]) && self::$_1746245758[___1531166732(54)][$_2050893273][round(0+0.66666666666667+0.66666666666667+0.66666666666667)]) return; $_1858180547= array(); if(isset(self::$_1398066271[$_2050893273]) && $GLOBALS['____781372918'][20](self::$_1398066271[$_2050893273])){ foreach(self::$_1398066271[$_2050893273] as $_667403107){ if(isset(self::$_1746245758[___1531166732(55)][$_667403107]) && self::$_1746245758[___1531166732(56)][$_667403107]){ self::$_1746245758[___1531166732(57)][$_667403107]= false; $_1858180547[]= array($_667403107, false);}} self::$_1746245758[___1531166732(58)][$_2050893273][round(0+0.5+0.5+0.5+0.5)]= true;} $_749944996= $GLOBALS['____781372918'][21](self::$_1746245758); $_749944996= $GLOBALS['____781372918'][22]($_749944996); COption::SetOptionString(___1531166732(59), ___1531166732(60), $_749944996); foreach($_1858180547 as $_884650181) self::__1894617401($_884650181[(164*2-328)], $_884650181[round(0+1)]);} public static function ModifyFeaturesSettings($_991293185, $_1814120840){ self::__1538967409(); foreach($_991293185 as $_2050893273 => $_1844152336) self::$_1746245758[___1531166732(61)][$_2050893273]= $_1844152336; $_1858180547= array(); foreach($_1814120840 as $_667403107 => $_1821722536){ if(!isset(self::$_1746245758[___1531166732(62)][$_667403107]) && $_1821722536 || isset(self::$_1746245758[___1531166732(63)][$_667403107]) && $_1821722536 != self::$_1746245758[___1531166732(64)][$_667403107]) $_1858180547[]= array($_667403107, $_1821722536); self::$_1746245758[___1531166732(65)][$_667403107]= $_1821722536;} $_749944996= $GLOBALS['____781372918'][23](self::$_1746245758); $_749944996= $GLOBALS['____781372918'][24]($_749944996); COption::SetOptionString(___1531166732(66), ___1531166732(67), $_749944996); self::$_1746245758= false; foreach($_1858180547 as $_884650181) self::__1894617401($_884650181[(219*2-438)], $_884650181[round(0+0.33333333333333+0.33333333333333+0.33333333333333)]);} public static function SaveFeaturesSettings($_317405048, $_53806888){ self::__1538967409(); $_9200224= array(___1531166732(68) => array(), ___1531166732(69) => array()); if(!$GLOBALS['____781372918'][25]($_317405048)) $_317405048= array(); if(!$GLOBALS['____781372918'][26]($_53806888)) $_53806888= array(); if(!$GLOBALS['____781372918'][27](___1531166732(70), $_317405048)) $_317405048[]= ___1531166732(71); foreach(self::$_1398066271 as $_2050893273 => $_1814120840){ if(isset(self::$_1746245758[___1531166732(72)][$_2050893273])){ $_1284816015= self::$_1746245758[___1531166732(73)][$_2050893273];} else{ $_1284816015=($_2050893273 == ___1531166732(74)? array(___1531166732(75)): array(___1531166732(76)));} if($_1284816015[(195*2-390)] == ___1531166732(77) || $_1284816015[(928-2*464)] == ___1531166732(78)){ $_9200224[___1531166732(79)][$_2050893273]= $_1284816015;} else{ if($GLOBALS['____781372918'][28]($_2050893273, $_317405048)) $_9200224[___1531166732(80)][$_2050893273]= array(___1531166732(81), $GLOBALS['____781372918'][29]((1444/2-722),(970-2*485),(1192/2-596), $GLOBALS['____781372918'][30](___1531166732(82)), $GLOBALS['____781372918'][31](___1531166732(83)), $GLOBALS['____781372918'][32](___1531166732(84)))); else $_9200224[___1531166732(85)][$_2050893273]= array(___1531166732(86));}} $_1858180547= array(); foreach(self::$_1645323014 as $_667403107 => $_2050893273){ if($_9200224[___1531166732(87)][$_2050893273][(1204/2-602)] != ___1531166732(88) && $_9200224[___1531166732(89)][$_2050893273][(782-2*391)] != ___1531166732(90)){ $_9200224[___1531166732(91)][$_667403107]= false;} else{ if($_9200224[___1531166732(92)][$_2050893273][(149*2-298)] == ___1531166732(93) && $_9200224[___1531166732(94)][$_2050893273][round(0+0.33333333333333+0.33333333333333+0.33333333333333)]< $GLOBALS['____781372918'][33]((154*2-308),(800-2*400),(850-2*425), Date(___1531166732(95)), $GLOBALS['____781372918'][34](___1531166732(96))- self::$_1455645276, $GLOBALS['____781372918'][35](___1531166732(97)))) $_9200224[___1531166732(98)][$_667403107]= false; else $_9200224[___1531166732(99)][$_667403107]= $GLOBALS['____781372918'][36]($_667403107, $_53806888); if(!isset(self::$_1746245758[___1531166732(100)][$_667403107]) && $_9200224[___1531166732(101)][$_667403107] || isset(self::$_1746245758[___1531166732(102)][$_667403107]) && $_9200224[___1531166732(103)][$_667403107] != self::$_1746245758[___1531166732(104)][$_667403107]) $_1858180547[]= array($_667403107, $_9200224[___1531166732(105)][$_667403107]);}} $_749944996= $GLOBALS['____781372918'][37]($_9200224); $_749944996= $GLOBALS['____781372918'][38]($_749944996); COption::SetOptionString(___1531166732(106), ___1531166732(107), $_749944996); self::$_1746245758= false; foreach($_1858180547 as $_884650181) self::__1894617401($_884650181[(226*2-452)], $_884650181[round(0+0.25+0.25+0.25+0.25)]);} public static function GetFeaturesList(){ self::__1538967409(); $_1210619625= array(); foreach(self::$_1398066271 as $_2050893273 => $_1814120840){ if(isset(self::$_1746245758[___1531166732(108)][$_2050893273])){ $_1284816015= self::$_1746245758[___1531166732(109)][$_2050893273];} else{ $_1284816015=($_2050893273 == ___1531166732(110)? array(___1531166732(111)): array(___1531166732(112)));} $_1210619625[$_2050893273]= array( ___1531166732(113) => $_1284816015[min(190,0,63.333333333333)], ___1531166732(114) => $_1284816015[round(0+1)], ___1531166732(115) => array(),); $_1210619625[$_2050893273][___1531166732(116)]= false; if($_1210619625[$_2050893273][___1531166732(117)] == ___1531166732(118)){ $_1210619625[$_2050893273][___1531166732(119)]= $GLOBALS['____781372918'][39](($GLOBALS['____781372918'][40]()- $_1210619625[$_2050893273][___1531166732(120)])/ round(0+28800+28800+28800)); if($_1210619625[$_2050893273][___1531166732(121)]> self::$_1455645276) $_1210619625[$_2050893273][___1531166732(122)]= true;} foreach($_1814120840 as $_667403107) $_1210619625[$_2050893273][___1531166732(123)][$_667403107]=(!isset(self::$_1746245758[___1531166732(124)][$_667403107]) || self::$_1746245758[___1531166732(125)][$_667403107]);} return $_1210619625;} private static function __957981141($_1848410016, $_928122234){ if(IsModuleInstalled($_1848410016) == $_928122234) return true; $_571522164= $_SERVER[___1531166732(126)].___1531166732(127).$_1848410016.___1531166732(128); if(!$GLOBALS['____781372918'][41]($_571522164)) return false; include_once($_571522164); $_21912240= $GLOBALS['____781372918'][42](___1531166732(129), ___1531166732(130), $_1848410016); if(!$GLOBALS['____781372918'][43]($_21912240)) return false; $_1396113624= new $_21912240; if($_928122234){ if(!$_1396113624->InstallDB()) return false; $_1396113624->InstallEvents(); if(!$_1396113624->InstallFiles()) return false;} else{ if(CModule::IncludeModule(___1531166732(131))) CSearch::DeleteIndex($_1848410016); UnRegisterModule($_1848410016);} return true;} protected static function OnRequestsSettingsChange($_667403107, $_1821722536){ self::__957981141("form", $_1821722536);} protected static function OnLearningSettingsChange($_667403107, $_1821722536){ self::__957981141("learning", $_1821722536);} protected static function OnJabberSettingsChange($_667403107, $_1821722536){ self::__957981141("xmpp", $_1821722536);} protected static function OnVideoConferenceSettingsChange($_667403107, $_1821722536){ self::__957981141("video", $_1821722536);} protected static function OnBizProcSettingsChange($_667403107, $_1821722536){ self::__957981141("bizprocdesigner", $_1821722536);} protected static function OnListsSettingsChange($_667403107, $_1821722536){ self::__957981141("lists", $_1821722536);} protected static function OnWikiSettingsChange($_667403107, $_1821722536){ self::__957981141("wiki", $_1821722536);} protected static function OnSupportSettingsChange($_667403107, $_1821722536){ self::__957981141("support", $_1821722536);} protected static function OnControllerSettingsChange($_667403107, $_1821722536){ self::__957981141("controller", $_1821722536);} protected static function OnAnalyticsSettingsChange($_667403107, $_1821722536){ self::__957981141("statistic", $_1821722536);} protected static function OnVoteSettingsChange($_667403107, $_1821722536){ self::__957981141("vote", $_1821722536);} protected static function OnFriendsSettingsChange($_667403107, $_1821722536){ if($_1821722536) $_1070075460= "Y"; else $_1070075460= ___1531166732(132); $_1135217124= CSite::GetList(___1531166732(133), ___1531166732(134), array(___1531166732(135) => ___1531166732(136))); while($_790105433= $_1135217124->Fetch()){ if(COption::GetOptionString(___1531166732(137), ___1531166732(138), ___1531166732(139), $_790105433[___1531166732(140)]) != $_1070075460){ COption::SetOptionString(___1531166732(141), ___1531166732(142), $_1070075460, false, $_790105433[___1531166732(143)]); COption::SetOptionString(___1531166732(144), ___1531166732(145), $_1070075460);}}} protected static function OnMicroBlogSettingsChange($_667403107, $_1821722536){ if($_1821722536) $_1070075460= "Y"; else $_1070075460= ___1531166732(146); $_1135217124= CSite::GetList(___1531166732(147), ___1531166732(148), array(___1531166732(149) => ___1531166732(150))); while($_790105433= $_1135217124->Fetch()){ if(COption::GetOptionString(___1531166732(151), ___1531166732(152), ___1531166732(153), $_790105433[___1531166732(154)]) != $_1070075460){ COption::SetOptionString(___1531166732(155), ___1531166732(156), $_1070075460, false, $_790105433[___1531166732(157)]); COption::SetOptionString(___1531166732(158), ___1531166732(159), $_1070075460);} if(COption::GetOptionString(___1531166732(160), ___1531166732(161), ___1531166732(162), $_790105433[___1531166732(163)]) != $_1070075460){ COption::SetOptionString(___1531166732(164), ___1531166732(165), $_1070075460, false, $_790105433[___1531166732(166)]); COption::SetOptionString(___1531166732(167), ___1531166732(168), $_1070075460);}}} protected static function OnPersonalFilesSettingsChange($_667403107, $_1821722536){ if($_1821722536) $_1070075460= "Y"; else $_1070075460= ___1531166732(169); $_1135217124= CSite::GetList(___1531166732(170), ___1531166732(171), array(___1531166732(172) => ___1531166732(173))); while($_790105433= $_1135217124->Fetch()){ if(COption::GetOptionString(___1531166732(174), ___1531166732(175), ___1531166732(176), $_790105433[___1531166732(177)]) != $_1070075460){ COption::SetOptionString(___1531166732(178), ___1531166732(179), $_1070075460, false, $_790105433[___1531166732(180)]); COption::SetOptionString(___1531166732(181), ___1531166732(182), $_1070075460);}}} protected static function OnPersonalBlogSettingsChange($_667403107, $_1821722536){ if($_1821722536) $_1070075460= "Y"; else $_1070075460= ___1531166732(183); $_1135217124= CSite::GetList(___1531166732(184), ___1531166732(185), array(___1531166732(186) => ___1531166732(187))); while($_790105433= $_1135217124->Fetch()){ if(COption::GetOptionString(___1531166732(188), ___1531166732(189), ___1531166732(190), $_790105433[___1531166732(191)]) != $_1070075460){ COption::SetOptionString(___1531166732(192), ___1531166732(193), $_1070075460, false, $_790105433[___1531166732(194)]); COption::SetOptionString(___1531166732(195), ___1531166732(196), $_1070075460);}}} protected static function OnPersonalPhotoSettingsChange($_667403107, $_1821722536){ if($_1821722536) $_1070075460= "Y"; else $_1070075460= ___1531166732(197); $_1135217124= CSite::GetList(___1531166732(198), ___1531166732(199), array(___1531166732(200) => ___1531166732(201))); while($_790105433= $_1135217124->Fetch()){ if(COption::GetOptionString(___1531166732(202), ___1531166732(203), ___1531166732(204), $_790105433[___1531166732(205)]) != $_1070075460){ COption::SetOptionString(___1531166732(206), ___1531166732(207), $_1070075460, false, $_790105433[___1531166732(208)]); COption::SetOptionString(___1531166732(209), ___1531166732(210), $_1070075460);}}} protected static function OnPersonalForumSettingsChange($_667403107, $_1821722536){ if($_1821722536) $_1070075460= "Y"; else $_1070075460= ___1531166732(211); $_1135217124= CSite::GetList(___1531166732(212), ___1531166732(213), array(___1531166732(214) => ___1531166732(215))); while($_790105433= $_1135217124->Fetch()){ if(COption::GetOptionString(___1531166732(216), ___1531166732(217), ___1531166732(218), $_790105433[___1531166732(219)]) != $_1070075460){ COption::SetOptionString(___1531166732(220), ___1531166732(221), $_1070075460, false, $_790105433[___1531166732(222)]); COption::SetOptionString(___1531166732(223), ___1531166732(224), $_1070075460);}}} protected static function OnTasksSettingsChange($_667403107, $_1821722536){ if($_1821722536) $_1070075460= "Y"; else $_1070075460= ___1531166732(225); $_1135217124= CSite::GetList(___1531166732(226), ___1531166732(227), array(___1531166732(228) => ___1531166732(229))); while($_790105433= $_1135217124->Fetch()){ if(COption::GetOptionString(___1531166732(230), ___1531166732(231), ___1531166732(232), $_790105433[___1531166732(233)]) != $_1070075460){ COption::SetOptionString(___1531166732(234), ___1531166732(235), $_1070075460, false, $_790105433[___1531166732(236)]); COption::SetOptionString(___1531166732(237), ___1531166732(238), $_1070075460);} if(COption::GetOptionString(___1531166732(239), ___1531166732(240), ___1531166732(241), $_790105433[___1531166732(242)]) != $_1070075460){ COption::SetOptionString(___1531166732(243), ___1531166732(244), $_1070075460, false, $_790105433[___1531166732(245)]); COption::SetOptionString(___1531166732(246), ___1531166732(247), $_1070075460);}} self::__957981141(___1531166732(248), $_1821722536);} protected static function OnCalendarSettingsChange($_667403107, $_1821722536){ if($_1821722536) $_1070075460= "Y"; else $_1070075460= ___1531166732(249); $_1135217124= CSite::GetList(___1531166732(250), ___1531166732(251), array(___1531166732(252) => ___1531166732(253))); while($_790105433= $_1135217124->Fetch()){ if(COption::GetOptionString(___1531166732(254), ___1531166732(255), ___1531166732(256), $_790105433[___1531166732(257)]) != $_1070075460){ COption::SetOptionString(___1531166732(258), ___1531166732(259), $_1070075460, false, $_790105433[___1531166732(260)]); COption::SetOptionString(___1531166732(261), ___1531166732(262), $_1070075460);} if(COption::GetOptionString(___1531166732(263), ___1531166732(264), ___1531166732(265), $_790105433[___1531166732(266)]) != $_1070075460){ COption::SetOptionString(___1531166732(267), ___1531166732(268), $_1070075460, false, $_790105433[___1531166732(269)]); COption::SetOptionString(___1531166732(270), ___1531166732(271), $_1070075460);}}} protected static function OnSMTPSettingsChange($_667403107, $_1821722536){ self::__957981141("mail", $_1821722536);} protected static function OnExtranetSettingsChange($_667403107, $_1821722536){ $_849027357= COption::GetOptionString("extranet", "extranet_site", ""); if($_849027357){ $_1923536333= new CSite; $_1923536333->Update($_849027357, array(___1531166732(272) =>($_1821722536? ___1531166732(273): ___1531166732(274))));} self::__957981141(___1531166732(275), $_1821722536);} protected static function OnDAVSettingsChange($_667403107, $_1821722536){ self::__957981141("dav", $_1821722536);} protected static function OntimemanSettingsChange($_667403107, $_1821722536){ self::__957981141("timeman", $_1821722536);} protected static function Onintranet_sharepointSettingsChange($_667403107, $_1821722536){ if($_1821722536){ RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "intranet", "CIntranetEventHandlers", "SPRegisterUpdatedItem"); RegisterModuleDependences(___1531166732(276), ___1531166732(277), ___1531166732(278), ___1531166732(279), ___1531166732(280)); CAgent::AddAgent(___1531166732(281), ___1531166732(282), ___1531166732(283), round(0+125+125+125+125)); CAgent::AddAgent(___1531166732(284), ___1531166732(285), ___1531166732(286), round(0+60+60+60+60+60)); CAgent::AddAgent(___1531166732(287), ___1531166732(288), ___1531166732(289), round(0+3600));} else{ UnRegisterModuleDependences(___1531166732(290), ___1531166732(291), ___1531166732(292), ___1531166732(293), ___1531166732(294)); UnRegisterModuleDependences(___1531166732(295), ___1531166732(296), ___1531166732(297), ___1531166732(298), ___1531166732(299)); CAgent::RemoveAgent(___1531166732(300), ___1531166732(301)); CAgent::RemoveAgent(___1531166732(302), ___1531166732(303)); CAgent::RemoveAgent(___1531166732(304), ___1531166732(305));}} protected static function OncrmSettingsChange($_667403107, $_1821722536){ if($_1821722536) COption::SetOptionString("crm", "form_features", "Y"); self::__957981141(___1531166732(306), $_1821722536);} protected static function OnClusterSettingsChange($_667403107, $_1821722536){ self::__957981141("cluster", $_1821722536);} protected static function OnMultiSitesSettingsChange($_667403107, $_1821722536){ if($_1821722536) RegisterModuleDependences("main", "OnBeforeProlog", "main", "CWizardSolPanelIntranet", "ShowPanel", 100, "/modules/intranet/panel_button.php"); else UnRegisterModuleDependences(___1531166732(307), ___1531166732(308), ___1531166732(309), ___1531166732(310), ___1531166732(311), ___1531166732(312));} protected static function OnIdeaSettingsChange($_667403107, $_1821722536){ self::__957981141("idea", $_1821722536);} protected static function OnMeetingSettingsChange($_667403107, $_1821722536){ self::__957981141("meeting", $_1821722536);} protected static function OnXDImportSettingsChange($_667403107, $_1821722536){ self::__957981141("xdimport", $_1821722536);}} $GLOBALS['____781372918'][44](___1531166732(313), ___1531166732(314));/**/			//Do not remove this

// Component 2.0 template engines
$GLOBALS['arCustomTemplateEngines'] = [];

// User fields manager
$GLOBALS['USER_FIELD_MANAGER'] = new CUserTypeManager;

// todo: remove global
$GLOBALS['BX_MENU_CUSTOM'] = CMenuCustom::getInstance();

if (file_exists(($_fname = __DIR__."/classes/general/update_db_updater.php")))
{
	$US_HOST_PROCESS_MAIN = false;
	include($_fname);
}

if (file_exists(($_fname = $_SERVER["DOCUMENT_ROOT"]."/bitrix/init.php")))
{
	include_once($_fname);
}

if (($_fname = getLocalPath("php_interface/init.php", BX_PERSONAL_ROOT)) !== false)
{
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);
}

if (($_fname = getLocalPath("php_interface/".SITE_ID."/init.php", BX_PERSONAL_ROOT)) !== false)
{
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);
}

//global var, is used somewhere
$GLOBALS["sDocPath"] = $GLOBALS["APPLICATION"]->GetCurPage();

if ((!(defined("STATISTIC_ONLY") && STATISTIC_ONLY && !str_starts_with($GLOBALS["APPLICATION"]->GetCurPage(), BX_ROOT . "/admin/"))) && COption::GetOptionString("main", "include_charset", "Y")=="Y" && LANG_CHARSET <> '')
{
	header("Content-Type: text/html; charset=".LANG_CHARSET);
}

if (COption::GetOptionString("main", "set_p3p_header", "Y")=="Y")
{
	header("P3P: policyref=\"/bitrix/p3p.xml\", CP=\"NON DSP COR CUR ADM DEV PSA PSD OUR UNR BUS UNI COM NAV INT DEM STA\"");
}

$license = $application->getLicense();
header("X-Powered-CMS: Bitrix Site Manager (" . ($license->isDemoKey() ? "DEMO" : $license->getPublicHashKey()) . ")");

if (COption::GetOptionString("main", "update_devsrv", "") == "Y")
{
	header("X-DevSrv-CMS: Bitrix");
}

//agents
if (COption::GetOptionString("main", "check_agents", "Y") == "Y")
{
	$application->addBackgroundJob(["CAgent", "CheckAgents"], [], \Bitrix\Main\Application::JOB_PRIORITY_LOW);
}

//send email events
if (COption::GetOptionString("main", "check_events", "Y") !== "N")
{
	$application->addBackgroundJob(['\Bitrix\Main\Mail\EventManager', 'checkEvents'], [], \Bitrix\Main\Application::JOB_PRIORITY_LOW-1);
}

$healerOfEarlySessionStart = new HealerEarlySessionStart();
$healerOfEarlySessionStart->process($application->getKernelSession());

$kernelSession = $application->getKernelSession();
$kernelSession->start();
$application->getSessionLocalStorageManager()->setUniqueId($kernelSession->getId());

foreach (GetModuleEvents("main", "OnPageStart", true) as $arEvent)
{
	ExecuteModuleEventEx($arEvent);
}

//define global user object
$GLOBALS["USER"] = new CUser;

//session control from group policy
$arPolicy = $GLOBALS["USER"]->GetSecurityPolicy();
$currTime = time();
if (
	(
		//IP address changed
		$kernelSession['SESS_IP']
		&& $arPolicy["SESSION_IP_MASK"] <> ''
		&& (
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($kernelSession['SESS_IP']))
			!=
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($_SERVER['REMOTE_ADDR']))
		)
	)
	||
	(
		//session timeout
		$arPolicy["SESSION_TIMEOUT"]>0
		&& $kernelSession['SESS_TIME']>0
		&& $currTime-$arPolicy["SESSION_TIMEOUT"]*60 > $kernelSession['SESS_TIME']
	)
	||
	(
		//signed session
		isset($kernelSession["BX_SESSION_SIGN"])
		&& $kernelSession["BX_SESSION_SIGN"] <> bitrix_sess_sign()
	)
	||
	(
		//session manually expired, e.g. in $User->LoginHitByHash
		isSessionExpired()
	)
)
{
	$compositeSessionManager = $application->getCompositeSessionManager();
	$compositeSessionManager->destroy();

	$application->getSession()->setId(Main\Security\Random::getString(32));
	$compositeSessionManager->start();

	$GLOBALS["USER"] = new CUser;
}
$kernelSession['SESS_IP'] = $_SERVER['REMOTE_ADDR'] ?? null;
if (empty($kernelSession['SESS_TIME']))
{
	$kernelSession['SESS_TIME'] = $currTime;
}
elseif (($currTime - $kernelSession['SESS_TIME']) > 60)
{
	$kernelSession['SESS_TIME'] = $currTime;
}
if (!isset($kernelSession["BX_SESSION_SIGN"]))
{
	$kernelSession["BX_SESSION_SIGN"] = bitrix_sess_sign();
}

//session control from security module
if (
	(COption::GetOptionString("main", "use_session_id_ttl", "N") == "Y")
	&& (COption::GetOptionInt("main", "session_id_ttl", 0) > 0)
	&& !defined("BX_SESSION_ID_CHANGE")
)
{
	if (!isset($kernelSession['SESS_ID_TIME']))
	{
		$kernelSession['SESS_ID_TIME'] = $currTime;
	}
	elseif (($kernelSession['SESS_ID_TIME'] + COption::GetOptionInt("main", "session_id_ttl")) < $kernelSession['SESS_TIME'])
	{
		$compositeSessionManager = $application->getCompositeSessionManager();
		$compositeSessionManager->regenerateId();

		$kernelSession['SESS_ID_TIME'] = $currTime;
	}
}

define("BX_STARTED", true);

if (isset($kernelSession['BX_ADMIN_LOAD_AUTH']))
{
	define('ADMIN_SECTION_LOAD_AUTH', 1);
	unset($kernelSession['BX_ADMIN_LOAD_AUTH']);
}

$bRsaError = false;
$USER_LID = false;

if (!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true)
{
	$doLogout = isset($_REQUEST["logout"]) && (strtolower($_REQUEST["logout"]) == "yes");

	if ($doLogout && $GLOBALS["USER"]->IsAuthorized())
	{
		$secureLogout = (\Bitrix\Main\Config\Option::get("main", "secure_logout", "N") == "Y");

		if (!$secureLogout || check_bitrix_sessid())
		{
			$GLOBALS["USER"]->Logout();
			LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam('', array('logout', 'sessid')));
		}
	}

	// authorize by cookies
	if (!$GLOBALS["USER"]->IsAuthorized())
	{
		$GLOBALS["USER"]->LoginByCookies();
	}

	$arAuthResult = false;

	//http basic and digest authorization
	if (($httpAuth = $GLOBALS["USER"]->LoginByHttpAuth()) !== null)
	{
		$arAuthResult = $httpAuth;
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}

	//Authorize user from authorization html form
	//Only POST is accepted
	if (isset($_POST["AUTH_FORM"]) && $_POST["AUTH_FORM"] <> '')
	{
		if (COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
		{
			//possible encrypted user password
			$sec = new CRsaSecurity();
			if (($arKeys = $sec->LoadKeys()))
			{
				$sec->SetKeys($arKeys);
				$errno = $sec->AcceptFromForm(['USER_PASSWORD', 'USER_CONFIRM_PASSWORD', 'USER_CURRENT_PASSWORD']);
				if ($errno == CRsaSecurity::ERROR_SESS_CHECK)
				{
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_sess"), "TYPE"=>"ERROR");
				}
				elseif ($errno < 0)
				{
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_err", array("#ERRCODE#"=>$errno)), "TYPE"=>"ERROR");
				}

				if ($errno < 0)
				{
					$bRsaError = true;
				}
			}
		}

		if (!$bRsaError)
		{
			if (!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
			{
				$USER_LID = SITE_ID;
			}

			$_POST["TYPE"] = $_POST["TYPE"] ?? null;
			if (isset($_POST["TYPE"]) && $_POST["TYPE"] == "AUTH")
			{
				$arAuthResult = $GLOBALS["USER"]->Login(
					$_POST["USER_LOGIN"] ?? '',
					$_POST["USER_PASSWORD"] ?? '',
					$_POST["USER_REMEMBER"] ?? ''
				);
			}
			elseif (isset($_POST["TYPE"]) && $_POST["TYPE"] == "OTP")
			{
				$arAuthResult = $GLOBALS["USER"]->LoginByOtp(
					$_POST["USER_OTP"] ?? '',
					$_POST["OTP_REMEMBER"] ?? '',
					$_POST["captcha_word"] ?? '',
					$_POST["captcha_sid"] ?? ''
				);
			}
			elseif (isset($_POST["TYPE"]) && $_POST["TYPE"] == "SEND_PWD")
			{
				$arAuthResult = CUser::SendPassword(
					$_POST["USER_LOGIN"] ?? '',
					$_POST["USER_EMAIL"] ?? '',
					$USER_LID,
					$_POST["captcha_word"] ?? '',
					$_POST["captcha_sid"] ?? '',
					$_POST["USER_PHONE_NUMBER"] ?? ''
				);
			}
			elseif (isset($_POST["TYPE"]) && $_POST["TYPE"] == "CHANGE_PWD")
			{
				$arAuthResult = $GLOBALS["USER"]->ChangePassword(
					$_POST["USER_LOGIN"] ?? '',
					$_POST["USER_CHECKWORD"] ?? '',
					$_POST["USER_PASSWORD"] ?? '',
					$_POST["USER_CONFIRM_PASSWORD"] ?? '',
					$USER_LID,
					$_POST["captcha_word"] ?? '',
					$_POST["captcha_sid"] ?? '',
					true,
					$_POST["USER_PHONE_NUMBER"] ?? '',
					$_POST["USER_CURRENT_PASSWORD"] ?? ''
				);
			}

			if ($_POST["TYPE"] == "AUTH" || $_POST["TYPE"] == "OTP")
			{
				//special login form in the control panel
				if ($arAuthResult === true && defined('ADMIN_SECTION') && ADMIN_SECTION === true)
				{
					//store cookies for next hit (see CMain::GetSpreadCookieHTML())
					$GLOBALS["APPLICATION"]->StoreCookies();
					$kernelSession['BX_ADMIN_LOAD_AUTH'] = true;

					// die() follows
					CMain::FinalActions('<script>window.onload=function(){(window.BX || window.parent.BX).AUTHAGENT.setAuthResult(false);};</script>');
				}
			}
		}
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}
	elseif (!$GLOBALS["USER"]->IsAuthorized() && isset($_REQUEST['bx_hit_hash']))
	{
		//Authorize by unique URL
		$GLOBALS["USER"]->LoginHitByHash($_REQUEST['bx_hit_hash']);
	}
}

//logout or re-authorize the user if something importand has changed
$GLOBALS["USER"]->CheckAuthActions();

//magic short URI
if (defined("BX_CHECK_SHORT_URI") && BX_CHECK_SHORT_URI && CBXShortUri::CheckUri())
{
	//local redirect inside
	die();
}

//application password scope control
if (($applicationID = $GLOBALS["USER"]->getContext()->getApplicationId()) !== null)
{
	$appManager = Main\Authentication\ApplicationManager::getInstance();
	if ($appManager->checkScope($applicationID) !== true)
	{
		$event = new Main\Event("main", "onApplicationScopeError", Array('APPLICATION_ID' => $applicationID));
		$event->send();

		$context->getResponse()->setStatus("403 Forbidden");
		$application->end();
	}
}

//define the site template
if (!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
{
	$siteTemplate = "";
	if (isset($_REQUEST["bitrix_preview_site_template"]) && is_string($_REQUEST["bitrix_preview_site_template"]) && $_REQUEST["bitrix_preview_site_template"] <> "" && $GLOBALS["USER"]->CanDoOperation('view_other_settings'))
	{
		//preview of site template
		$signer = new Bitrix\Main\Security\Sign\Signer();
		try
		{
			//protected by a sign
			$requestTemplate = $signer->unsign($_REQUEST["bitrix_preview_site_template"], "template_preview".bitrix_sessid());

			$aTemplates = CSiteTemplate::GetByID($requestTemplate);
			if ($template = $aTemplates->Fetch())
			{
				$siteTemplate = $template["ID"];

				//preview of unsaved template
				if (isset($_GET['bx_template_preview_mode']) && $_GET['bx_template_preview_mode'] == 'Y' && $GLOBALS["USER"]->CanDoOperation('edit_other_settings'))
				{
					define("SITE_TEMPLATE_PREVIEW_MODE", true);
				}
			}
		}
		catch(\Bitrix\Main\Security\Sign\BadSignatureException $e)
		{
		}
	}
	if ($siteTemplate == "")
	{
		$siteTemplate = CSite::GetCurTemplate();
	}

	if (!defined('SITE_TEMPLATE_ID'))
	{
		define("SITE_TEMPLATE_ID", $siteTemplate);
	}

	define("SITE_TEMPLATE_PATH", getLocalPath('templates/'.SITE_TEMPLATE_ID, BX_PERSONAL_ROOT));
}
else
{
	// prevents undefined constants
	if (!defined('SITE_TEMPLATE_ID'))
	{
		define('SITE_TEMPLATE_ID', '.default');
	}

	define('SITE_TEMPLATE_PATH', '/bitrix/templates/.default');
}

//magic parameters: show page creation time
if (isset($_GET["show_page_exec_time"]))
{
	if ($_GET["show_page_exec_time"]=="Y" || $_GET["show_page_exec_time"]=="N")
	{
		$kernelSession["SESS_SHOW_TIME_EXEC"] = $_GET["show_page_exec_time"];
	}
}

//magic parameters: show included file processing time
if (isset($_GET["show_include_exec_time"]))
{
	if ($_GET["show_include_exec_time"]=="Y" || $_GET["show_include_exec_time"]=="N")
	{
		$kernelSession["SESS_SHOW_INCLUDE_TIME_EXEC"] = $_GET["show_include_exec_time"];
	}
}

//magic parameters: show include areas
if (isset($_GET["bitrix_include_areas"]) && $_GET["bitrix_include_areas"] <> "")
{
	$GLOBALS["APPLICATION"]->SetShowIncludeAreas($_GET["bitrix_include_areas"]=="Y");
}

//magic sound
if ($GLOBALS["USER"]->IsAuthorized())
{
	$cookie_prefix = COption::GetOptionString('main', 'cookie_name', 'BITRIX_SM');
	if (!isset($_COOKIE[$cookie_prefix.'_SOUND_LOGIN_PLAYED']))
	{
		$GLOBALS["APPLICATION"]->set_cookie('SOUND_LOGIN_PLAYED', 'Y', 0);
	}
}

//magic cache
\Bitrix\Main\Composite\Engine::shouldBeEnabled();

// should be before proactive filter on OnBeforeProlog
$userPassword = $_POST["USER_PASSWORD"] ?? null;
$userConfirmPassword = $_POST["USER_CONFIRM_PASSWORD"] ?? null;

foreach(GetModuleEvents("main", "OnBeforeProlog", true) as $arEvent)
{
	ExecuteModuleEventEx($arEvent);
}

if (!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS !== true)
{
	//Register user from authorization html form
	//Only POST is accepted
	if (isset($_POST["AUTH_FORM"]) && $_POST["AUTH_FORM"] != '' && isset($_POST["TYPE"]) && $_POST["TYPE"] == "REGISTRATION")
	{
		if (!$bRsaError)
		{
			if (COption::GetOptionString("main", "new_user_registration", "N") == "Y" && (!defined("ADMIN_SECTION") || ADMIN_SECTION !== true))
			{
				$arAuthResult = $GLOBALS["USER"]->Register(
					$_POST["USER_LOGIN"] ?? '',
					$_POST["USER_NAME"] ?? '',
					$_POST["USER_LAST_NAME"] ?? '',
					$userPassword,
					$userConfirmPassword,
					$_POST["USER_EMAIL"] ?? '',
					$USER_LID,
					$_POST["captcha_word"] ?? '',
					$_POST["captcha_sid"] ?? '',
					false,
					$_POST["USER_PHONE_NUMBER"] ?? ''
				);

				$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
			}
		}
	}
}

if ((!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true) && (!defined("NOT_CHECK_FILE_PERMISSIONS") || NOT_CHECK_FILE_PERMISSIONS!==true))
{
	$real_path = $context->getRequest()->getScriptFile();

	if (!$GLOBALS["USER"]->CanDoFileOperation('fm_view_file', array(SITE_ID, $real_path)) || (defined("NEED_AUTH") && NEED_AUTH && !$GLOBALS["USER"]->IsAuthorized()))
	{
		if ($GLOBALS["USER"]->IsAuthorized() && $arAuthResult["MESSAGE"] == '')
		{
			$arAuthResult = array("MESSAGE"=>GetMessage("ACCESS_DENIED").' '.GetMessage("ACCESS_DENIED_FILE", array("#FILE#"=>$real_path)), "TYPE"=>"ERROR");

			if (COption::GetOptionString("main", "event_log_permissions_fail", "N") === "Y")
			{
				CEventLog::Log("SECURITY", "USER_PERMISSIONS_FAIL", "main", $GLOBALS["USER"]->GetID(), $real_path);
			}
		}

		if (defined("ADMIN_SECTION") && ADMIN_SECTION === true)
		{
			if (isset($_REQUEST["mode"]) && ($_REQUEST["mode"] === "list" || $_REQUEST["mode"] === "settings"))
			{
				echo "<script>top.location='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';</script>";
				die();
			}
			elseif (isset($_REQUEST["mode"]) && $_REQUEST["mode"] === "frame")
			{
				echo "<script>
					var w = (opener? opener.window:parent.window);
					w.location.href='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';
				</script>";
				die();
			}
			elseif (defined("MOBILE_APP_ADMIN") && MOBILE_APP_ADMIN === true)
			{
				echo json_encode(Array("status"=>"failed"));
				die();
			}
		}

		/** @noinspection PhpUndefinedVariableInspection */
		$GLOBALS["APPLICATION"]->AuthForm($arAuthResult);
	}
}

/*ZDUyZmZOTJhNzljMzQ3NjExM2M3MzljYTUzOWFlMjFhOGIwMTQ=*/$GLOBALS['____1997940576']= array(base64_decode(''.'bX'.'R'.'fcmF'.'uZA'.'=='),base64_decode('ZX'.'h'.'wbG9kZQ='.'='),base64_decode(''.'cGFj'.'aw=='),base64_decode('b'.'WQ1'),base64_decode('Y29uc3Rh'.'bnQ='),base64_decode('a'.'GFza'.'F9o'.'bW'.'Fj'),base64_decode('c3RyY21w'),base64_decode('a'.'X'.'N'.'f'.'b2JqZWN0'),base64_decode('Y2Fs'.'bF91'.'c2V'.'yX2'.'Z1'.'bmM='),base64_decode(''.'Y'.'2Fs'.'bF'.'91c2VyX2Z1bmM='),base64_decode('Y'.'2FsbF91'.'c2'.'V'.'yX2Z1bmM='),base64_decode('Y2FsbF91c'.'2VyX'.'2Z1'.'bm'.'M='),base64_decode('Y2FsbF9'.'1c2VyX2Z'.'1bmM='));if(!function_exists(__NAMESPACE__.'\\___1716263523')){function ___1716263523($_1540753772){static $_93267511= false; if($_93267511 == false) $_93267511=array(''.'R'.'EI=','U'.'0VM'.'RUNU'.'IFZBTF'.'VFIEZST00gY'.'l9vc'.'HRpb24'.'gV0hFUk'.'U'.'gTkFNRT0'.'nf'.'lBB'.'UkFNX0'.'1BWF9V'.'U0V'.'SUycg'.'QU5EI'.'E1P'.'RFVMR'.'V9'.'JRD0n'.'b'.'WFpb'.'i'.'c'.'gQU5E'.'IFNJVEVfSUQgSV'.'MgTlVMTA='.'=','VkFMVUU=','L'.'g==','SCo=','Yml0cml4','TElDR'.'U5T'.'RV9L'.'RVk=','c2hh'.'MjU2','VVNFUg'.'==','VVNF'.'Ug==','VVN'.'FUg==','S'.'XNBdXRob3'.'JpemV'.'k',''.'VVNFUg==','SXNBZG1'.'pbg'.'='.'=','QVBQTElDQVRJT04=','U'.'mVz'.'dGFydEJ1ZmZl'.'cg==',''.'TG9jYWxS'.'ZW'.'R'.'p'.'cmVjdA='.'=',''.'L2x'.'pY2'.'Vuc2VfcmVzdHJp'.'Y3Rpb24'.'ucGhw','XEJ'.'pd'.'HJpeFx'.'N'.'YWluXENvbmZpZ1xPcHR'.'pb246OnNld'.'A='.'=',''.'bW'.'Fpbg==','UEFSQU1fTUF'.'Y'.'X1VT'.'RVJT');return base64_decode($_93267511[$_1540753772]);}};if($GLOBALS['____1997940576'][0](round(0+0.2+0.2+0.2+0.2+0.2), round(0+5+5+5+5)) == round(0+2.3333333333333+2.3333333333333+2.3333333333333)){ $_1314036361= $GLOBALS[___1716263523(0)]->Query(___1716263523(1), true); if($_1159536275= $_1314036361->Fetch()){ $_1066702976= $_1159536275[___1716263523(2)]; list($_83507701, $_1552659236)= $GLOBALS['____1997940576'][1](___1716263523(3), $_1066702976); $_985745072= $GLOBALS['____1997940576'][2](___1716263523(4), $_83507701); $_1327239057= ___1716263523(5).$GLOBALS['____1997940576'][3]($GLOBALS['____1997940576'][4](___1716263523(6))); $_1583773935= $GLOBALS['____1997940576'][5](___1716263523(7), $_1552659236, $_1327239057, true); if($GLOBALS['____1997940576'][6]($_1583773935, $_985745072) !== min(180,0,60)){ if(isset($GLOBALS[___1716263523(8)]) && $GLOBALS['____1997940576'][7]($GLOBALS[___1716263523(9)]) && $GLOBALS['____1997940576'][8](array($GLOBALS[___1716263523(10)], ___1716263523(11))) &&!$GLOBALS['____1997940576'][9](array($GLOBALS[___1716263523(12)], ___1716263523(13)))){ $GLOBALS['____1997940576'][10](array($GLOBALS[___1716263523(14)], ___1716263523(15))); $GLOBALS['____1997940576'][11](___1716263523(16), ___1716263523(17), true);}}} else{ $GLOBALS['____1997940576'][12](___1716263523(18), ___1716263523(19), ___1716263523(20), round(0+4+4+4));}}/**/       //Do not remove this

