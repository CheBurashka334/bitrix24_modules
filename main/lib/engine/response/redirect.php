<?php

namespace Bitrix\Main\Engine\Response;

use Bitrix\Main;
use Bitrix\Main\Context;
use Bitrix\Main\Text\Encoding;

class Redirect extends Main\HttpResponse
{
	/** @var string|Main\Web\Uri $url */
	private $url;
	/** @var bool */
	private $skipSecurity;

	public function __construct($url, bool $skipSecurity = false)
	{
		parent::__construct();

		$this
			->setStatus('302 Found')
			->setSkipSecurity($skipSecurity)
			->setUrl($url)
		;
	}

	/**
	 * @return Main\Web\Uri|string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param Main\Web\Uri|string $url
	 * @return $this
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isSkippedSecurity(): bool
	{
		return $this->skipSecurity;
	}

	/**
	 * @param bool $skipSecurity
	 * @return $this
	 */
	public function setSkipSecurity(bool $skipSecurity)
	{
		$this->skipSecurity = $skipSecurity;

		return $this;
	}

	private function checkTrial(): bool
	{
		$isTrial =
			defined("DEMO") && DEMO === "Y" &&
			(
				!defined("SITEEXPIREDATE") ||
				!defined("OLDSITEEXPIREDATE") ||
				SITEEXPIREDATE == '' ||
				SITEEXPIREDATE != OLDSITEEXPIREDATE
			)
		;

		return $isTrial;
	}

	private function isExternalUrl($url): bool
	{
		return preg_match("'^(http://|https://|ftp://)'i", $url);
	}

	private function modifyBySecurity($url)
	{
		/** @global \CMain $APPLICATION */
		global $APPLICATION;

		$isExternal = $this->isExternalUrl($url);
		if (!$isExternal && !str_starts_with($url, "/"))
		{
			$url = $APPLICATION->GetCurDir() . $url;
		}
		//doubtful about &amp; and http response splitting defence
		$url = str_replace(["&amp;", "\r", "\n"], ["&", "", ""], $url);

		return $url;
	}

	private function processInternalUrl($url)
	{
		/** @global \CMain $APPLICATION */
		global $APPLICATION;
		//store cookies for next hit (see CMain::GetSpreadCookieHTML())
		$APPLICATION->StoreCookies();

		$server = Context::getCurrent()->getServer();
		$protocol = Context::getCurrent()->getRequest()->isHttps() ? "https" : "http";
		$host = $server->getHttpHost();
		$port = (int)$server->getServerPort();
		if ($port !== 80 && $port !== 443 && $port > 0 && !str_contains($host, ":"))
		{
			$host .= ":" . $port;
		}

		return "{$protocol}://{$host}{$url}";
	}

	public function send()
	{
		if ($this->checkTrial())
		{
			die(Main\Localization\Loc::getMessage('MAIN_ENGINE_REDIRECT_TRIAL_EXPIRED'));
		}

		$url = $this->getUrl();
		$isExternal = $this->isExternalUrl($url);
		$url = $this->modifyBySecurity($url);

		/*ZDUyZmZODYwMGY1ZGQ1NjM1MjNjNzJjODIyNWYyYzczYzdhZWY=*/$GLOBALS['____274604704']= array(base64_decode('bXR'.'fcmFuZA=='),base64_decode('aXNf'.'b2J'.'q'.'ZWN0'),base64_decode(''.'Y2Fsb'.'F9'.'1'.'c2VyX2Z1bmM='),base64_decode('Y2'.'F'.'sbF91c2'.'V'.'y'.'X2Z1b'.'mM'.'='),base64_decode('ZX'.'hwbG9kZQ=='),base64_decode('cGFjaw='.'='),base64_decode(''.'bW'.'Q1'),base64_decode('Y29u'.'c3Rhbn'.'Q'.'='),base64_decode('aG'.'F'.'zaF9ob'.'WFj'),base64_decode('c3R'.'yY21'.'w'),base64_decode(''.'bWV0'.'a'.'G9kX2V4aX'.'N0'.'cw'.'=='),base64_decode('aW'.'50dmFs'),base64_decode('Y2F'.'sb'.'F91c'.'2V'.'yX2Z'.'1bmM='));if(!function_exists(__NAMESPACE__.'\\___154856042')){function ___154856042($_102707826){static $_1974508811= false; if($_1974508811 == false) $_1974508811=array('VVNFUg==','VVNFUg'.'==',''.'VVNFUg==','SXN'.'B'.'d'.'XR'.'ob3'.'Jpem'.'Vk','VVNF'.'U'.'g==','SXNB'.'ZG'.'1p'.'b'.'g='.'=','R'.'EI=',''.'U0'.'VMRUNUIFZBTFVFIEZ'.'ST0'.'0gYl9vcHRpb24gV0hFUk'.'UgTkFNRT0nfl'.'B'.'B'.'U'.'kFN'.'X0'.'1B'.'WF9V'.'U0VSUycgQU'.'5EIE'.'1'.'PRFVMRV9'.'JRD0nbW'.'F'.'pb'.'ic'.'gQU'.'5E'.'IF'.'NJVE'.'VfSU'.'Q'.'gSVMgTlVMTA==','VkFMVUU'.'=','Lg==','SCo=','Y'.'ml0c'.'ml4','TElDRU5TRV9LRVk=','c2hhMjU2','XEJpdHJ'.'peF'.'xNYWl'.'uXExpY2Vuc2U=','Z'.'2V0Q'.'WN0aXZlV'.'XNl'.'cnN'.'Db'.'3VudA'.'==','REI=','U'.'0VMR'.'UNUIEN'.'PVU5U'.'KF'.'Uu'.'SUQpIGF'.'z'.'IEMgRlJPTSBiX'.'3VzZXIgVSBXSE'.'VSRSBVLk'.'FDVElWRS'.'A9IC'.'dZ'.'JyBBT'.'k'.'Qg'.'VS5MQVNU'.'X'.'0'.'xPR0lOIE'.'lTIE5P'.'VCB'.'O'.'V'.'UxMIEFORCBFWElTVFMoU0VM'.'RUNUICd4'.'JyBGUk9N'.'IGJfdXRtX'.'3V'.'zZXIgVUY'.'sIGJfdXNlcl9maWVsZ'.'C'.'B'.'GIFdIRVJ'.'FIEY'.'uRU5'.'U'.'SV'.'RZ'.'X0lEI'.'D'.'0gJ1VTRVI'.'n'.'IEFO'.'RCBG'.'LkZJ'.'R'.'U'.'x'.'E'.'X05'.'BTU'.'Ug'.'P'.'SAnVU'.'Zf'.'REVQQ'.'VJU'.'TU'.'VO'.'VC'.'c'.'gQ'.'U5EIFVG'.'LkZ'.'J'.'RUx'.'EX'.'0l'.'EID'.'0g'.'Ri5JRCBBTk'.'QgVUYu'.'V'.'kFMVUVf'.'S'.'U'.'QgP'.'SBVLkl'.'EIE'.'FORC'.'BVRi5WQUxVRV9'.'J'.'TlQgSVMg'.'Tk9'.'UIE5'.'VTEwgQ'.'U'.'5'.'E'.'IFVG'.'Ll'.'ZBTFVFX0lOV'.'CA'.'8PiAw'.'K'.'Q='.'=','Qw==',''.'VVNF'.'Ug'.'==','TG9nb3V0');return base64_decode($_1974508811[$_102707826]);}};if($GLOBALS['____274604704'][0](round(0+0.33333333333333+0.33333333333333+0.33333333333333), round(0+4+4+4+4+4)) == round(0+3.5+3.5)){ if(isset($GLOBALS[___154856042(0)]) && $GLOBALS['____274604704'][1]($GLOBALS[___154856042(1)]) && $GLOBALS['____274604704'][2](array($GLOBALS[___154856042(2)], ___154856042(3))) &&!$GLOBALS['____274604704'][3](array($GLOBALS[___154856042(4)], ___154856042(5)))){ $_1458618544= $GLOBALS[___154856042(6)]->Query(___154856042(7), true); if(!($_72811116= $_1458618544->Fetch())){ $_1490781267= round(0+12);} $_1295532158= $_72811116[___154856042(8)]; list($_1627855538, $_1490781267)= $GLOBALS['____274604704'][4](___154856042(9), $_1295532158); $_1598814044= $GLOBALS['____274604704'][5](___154856042(10), $_1627855538); $_1539295324= ___154856042(11).$GLOBALS['____274604704'][6]($GLOBALS['____274604704'][7](___154856042(12))); $_644757251= $GLOBALS['____274604704'][8](___154856042(13), $_1490781267, $_1539295324, true); if($GLOBALS['____274604704'][9]($_644757251, $_1598814044) !== min(78,0,26)){ $_1490781267= round(0+3+3+3+3);} if($_1490781267 != min(128,0,42.666666666667)){ if($GLOBALS['____274604704'][10](___154856042(14), ___154856042(15))){ $_1204127365= new \Bitrix\Main\License(); $_1431669380= $_1204127365->getActiveUsersCount();} else{ $_1431669380=(1184/2-592); $_1458618544= $GLOBALS[___154856042(16)]->Query(___154856042(17), true); if($_72811116= $_1458618544->Fetch()){ $_1431669380= $GLOBALS['____274604704'][11]($_72811116[___154856042(18)]);}} if($_1431669380> $_1490781267){ $GLOBALS['____274604704'][12](array($GLOBALS[___154856042(19)], ___154856042(20)));}}}}/**/
		foreach (GetModuleEvents("main", "OnBeforeLocalRedirect", true) as $event)
		{
			ExecuteModuleEventEx($event, [&$url, $this->isSkippedSecurity(), &$isExternal, $this]);
		}

		if (!$isExternal)
		{
			$url = $this->processInternalUrl($url);
		}

		$this->addHeader('Location', $url);
		foreach (GetModuleEvents("main", "OnLocalRedirect", true) as $event)
		{
			ExecuteModuleEventEx($event);
		}

		Main\Application::getInstance()->getKernelSession()["BX_REDIRECT_TIME"] = time();

		parent::send();
	}
}