import {Dom, Event, Runtime, Tag} from 'main.core';

export class FixFontSize
{
	constructor(params)
	{
		this.node = null;
		this.prevWindowSize = 0;
		this.prevWrapperSize = 0;
		this.mainWrapper = null;
		this.textWrapper = null;
		this.objList = params.objList;
		this.minFontSizeList = [];
		this.minFontSize = 0;

		if (params.onresize)
		{
			this.prevWindowSize = window.innerWidth || document.documentElement.clientWidth;
			Event.bind(window, 'resize', Runtime.throttle(this.onResize, 350, this));
		}

		if (params.onAdaptiveResize)
		{
			const widthNode = this.objList[0].scaleBy || this.objList[0].node;
			const {width, paddingLeft, paddingRight} = getComputedStyle(widthNode);

			this.prevWrapperSize = parseInt(width) - parseInt(paddingLeft) - parseInt(paddingRight);
			Event.bind(window, 'resize', Runtime.throttle(this.onAdaptiveResize, 350, this));
		}

		this.createTestNodes();
		this.decrease();
	}

	static init(params)
	{
		return new FixFontSize(params);
	}

	createTestNodes()
	{
		this.mainWrapper = Tag.render`
			<div style="height: 0; overflow: hidden;">
				<div style="display: inline-block; white-space: nowrap;"></div>
			</div>
		`;

		this.textWrapper = this.mainWrapper.firstElementChild;
	}

	insertTestNodes()
	{
		Dom.remove(this.mainWrapper);
	}

	removeTestNodes()
	{
		Dom.remove(this.mainWrapper);
	}

	decrease()
	{
		this.insertTestNodes();

		for (let i = this.objList.length - 1; i >= 0; i--)
		{
			const widthNode = this.objList[i].scaleBy || this.objList[i].node;
			const computedStyles = getComputedStyle(widthNode);
			const width  = parseInt(computedStyles["width"]) - parseInt(computedStyles["paddingLeft"]) - parseInt(computedStyles["paddingRight"]);
			let fontSize = parseInt(getComputedStyle(this.objList[i].node)["font-size"]);

			this.textWrapperSetStyle(this.objList[i].node);

			if (this.textWrapperInsertText(this.objList[i].node))
			{
				while (this.textWrapper.offsetWidth > width && fontSize > 0)
				{
					this.textWrapper.style.fontSize = --fontSize + 'px';
				}

				if (this.objList[i].smallestValue)
				{
					this.minFontSize = this.minFontSize ? Math.min(this.minFontSize, fontSize) : fontSize;
					this.minFontSizeList.push(this.objList[i].node)
				}
				else
				{
					this.objList[i].node.style.fontSize = fontSize + 'px';
				}
			}
		}

		if (this.minFontSizeList.length > 0)
		{
			this.setMinFont();
		}

		this.removeTestNodes();
	}

	increase()
	{
		this.insertTestNodes();
		this.insertTestNodes();

		for (let i = this.objList.length - 1; i >= 0; i--)
		{
			const widthNode = this.objList[i].scaleBy || this.objList[i].node;
			const computedStyles = getComputedStyle(widthNode);
			const width  = parseInt(computedStyles["width"]) - parseInt(computedStyles["paddingLeft"]) - parseInt(computedStyles["paddingRight"]);
			let fontSize = parseInt(getComputedStyle(this.objList[i].node)["font-size"]);

			this.textWrapperSetStyle(this.objList[i].node);

			if (this.textWrapperInsertText(this.objList[i].node))
			{
				while (this.textWrapper.offsetWidth < width && fontSize < this.objList[i].maxFontSize)
				{
					this.textWrapper.style.fontSize = ++fontSize + 'px';
				}

				fontSize--;

				if (this.objList[i].smallestValue)
				{
					this.minFontSize = this.minFontSize ? Math.min(this.minFontSize, fontSize) : fontSize;

					this.minFontSizeList.push(this.objList[i].node)
				}
				else
				{
					this.objList[i].node.style.fontSize = fontSize + 'px';
				}
			}
		}

		if (this.minFontSizeList.length > 0)
		{
			this.setMinFont();
		}

		this.removeTestNodes();
	}

	setMinFont()
	{
		for (let i = this.minFontSizeList.length - 1; i >= 0; i--)
		{
			this.minFontSizeList[i].style.fontSize = this.minFontSize + 'px';
		}

		this.minFontSize = 0;
	}

	onResize()
	{
		const width = window.innerWidth || document.documentElement.clientWidth;

		if (this.prevWindowSize > width)
		{
			this.decrease();
		}
		else if (this.prevWindowSize < width)
		{
			this.increase();
		}

		this.prevWindowSize = width;
	}

	onAdaptiveResize()
	{
		const widthNode = this.objList[0].scaleBy || this.objList[0].node;
		const {width, paddingLeft, paddingRight} = getComputedStyle(widthNode);
		const outerWidth = parseInt(width) - parseInt(paddingLeft) - parseInt(paddingRight);

		if (this.prevWrapperSize > outerWidth)
		{
			this.decrease();
		}
		else if (this.prevWrapperSize < outerWidth)
		{
			this.increase();
		}

		this.prevWrapperSize = outerWidth;
	}

	textWrapperInsertText(node)
	{
		if (node.textContent)
		{
			this.textWrapper.textContent = node.textContent;
			return true;
		}

		if (node.innerText)
		{
			this.textWrapper.innerText = node.innerText;
			return true;
		}

		return false;
	}

	textWrapperSetStyle(node)
	{
		const computedStyle = getComputedStyle(node);

		requestAnimationFrame(() => {
			Tag.style(this.textWrapper)`
				font-family: ${computedStyle.getPropertyValue('font-family')};
				font-size: ${computedStyle.getPropertyValue('font-size')};
				font-style: ${computedStyle.getPropertyValue('font-style')};
				font-weight: ${computedStyle.getPropertyValue('font-weight')};
				line-height: ${computedStyle.getPropertyValue('line-height')};
			`;
		});
	}
}